package io.easyspring.framework.common.exception;

import io.easyspring.framework.common.enums.ErrorResultEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 用户权限错误的异常类
 *
 * @author summer
 * DateTime 2019-01-16 14:45
 * @version V1.0.0-RELEASE
 */
@NoArgsConstructor
@Getter
public class AuthorizeException extends CommonException {

    /**
     * 权限权限异常的构造方法
     *
     * @param message 异常信息
     * Author summer
     * Version 2.0.0-RELEASE
     * DateTime 2019/12/5 9:37 下午
     */
    public AuthorizeException(String message){
        super(message);
        this.setCode(ErrorResultEnum.USER_PERMISSION_DENIED.getCode());
    }

    /**
     * 权限权限异常的构造方法
     *
     * @param message 异常信息
     * Author summer
     * Version 2.0.0-RELEASE
     * DateTime 2019/12/5 9:37 下午
     */
    public AuthorizeException(String message, Object detail){
        super(message);
        this.setDetails(detail);
        this.setCode(ErrorResultEnum.USER_PERMISSION_DENIED.getCode());
    }
}
